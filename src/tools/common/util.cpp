// Copyright (c) 2014-2020, The Monero Project
// 
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without modification, are
// permitted provided that the following conditions are met:
// 
// 1. Redistributions of source code must retain the above copyright notice, this list of
//    conditions and the following disclaimer.
// 
// 2. Redistributions in binary form must reproduce the above copyright notice, this list
//    of conditions and the following disclaimer in the documentation and/or other
//    materials provided with the distribution.
// 
// 3. Neither the name of the copyright holder nor the names of its contributors may be
//    used to endorse or promote products derived from this software without specific
//    prior written permission.
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
// EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
// MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL
// THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
// STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF
// THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// Parts of this file are originally copyright (c) 2012-2013 The Cryptonote developers

#include <unistd.h>
#include <cstdio>
#include <wchar.h>

#ifdef __GLIBC__
#include <gnu/libc-version.h>
#include <sys/sysmacros.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <ctype.h>
#endif

#include <string>
#include <iomanip>
#include <sstream>
#include <fstream>


#include "include_base_utils.h"
#include "file_io_utils.h"
#include "wipeable_string.h"
#include "misc_os_dependent.h"
using namespace epee;

#include "crypto/crypto.h"
#include "util.h"
#include "stack_trace.h"
#include "memwipe.h"
#include "net/http_client.h"                        // epee::net_utils::...
#include "readline_buffer.h"

#include <sys/file.h>
#include <sys/utsname.h>
#include <sys/stat.h>

#include <filesystem>
#include <boost/algorithm/string.hpp>
#include <boost/asio.hpp>
#include <boost/format.hpp>
#include <openssl/sha.h>

#undef MONERO_DEFAULT_LOG_CATEGORY
#define MONERO_DEFAULT_LOG_CATEGORY "util"

namespace
{

static int flock_exnb(int fd)
{
  struct flock fl;
  int ret;

  memset(&fl, 0, sizeof(fl));
  fl.l_type = F_WRLCK;
  fl.l_whence = SEEK_SET;
  fl.l_start = 0;
  fl.l_len = 0;
  ret = fcntl(fd, F_SETLK, &fl);
  if (ret < 0)
    MERROR("Error locking fd " << fd << ": " << errno << " (" << strerror(errno) << ")");
  return ret;
}

}

namespace tools
{
  std::function<void(int)> signal_handler::m_handler;

  file_locker::file_locker(const std::string &filename)
  {
    m_fd = open(filename.c_str(), O_RDWR | O_CREAT | O_CLOEXEC, 0666);
    if (m_fd != -1)
    {
      if (flock_exnb(m_fd) == -1)
      {
        MERROR("Failed to lock " << filename << ": " << std::strerror(errno));
        close(m_fd);
        m_fd = -1;
      }
    }
    else
    {
      MERROR("Failed to open " << filename << ": " << std::strerror(errno));
    }
  }
  file_locker::~file_locker()
  {
    if (locked())
    {
      close(m_fd);
    }
  }
  bool file_locker::locked() const
  {
    return m_fd != -1;
  }

  std::string get_nix_version_display_string()
  {
    struct utsname un;

    if(uname(&un) < 0)
      return std::string("*nix: failed to get os version");
    return std::string() + un.sysname + " " + un.version + " " + un.release;
  }

  std::string get_os_version_string()
  {
    return get_nix_version_display_string();
  }



  std::string get_default_data_dir()
  {
    /* Please for the love of god refactor  the ifdefs out of this */

    // namespace fs = std::filesystem;
    // Windows < Vista: C:\Documents and Settings\Username\Application Data\CRYPTONOTE_NAME
    // Windows >= Vista: C:\Users\Username\AppData\Roaming\CRYPTONOTE_NAME
    // Unix & Mac: ~/.CRYPTONOTE_NAME
    std::string config_folder;

    std::string pathRet;
    char* pszHome = getenv("HOME");
    if (pszHome == NULL || strlen(pszHome) == 0)
      pathRet = "/";
    else
      pathRet = pszHome;
    config_folder = (pathRet + "/." + CRYPTONOTE_NAME);

    return config_folder;
  }

  std::string get_default_log_file()
  {
    return (std::filesystem::path("/dev/null")).string();
  }

  bool create_directories_if_necessary(const std::string& path)
  {
    namespace fs = std::filesystem;
    std::error_code ec;
    fs::path fs_path(path);
    if (fs::is_directory(fs_path, ec))
    {
      return true;
    }

    bool res = fs::create_directories(fs_path, ec);
    if (res)
    {
      LOG_PRINT_L2("Created directory: " << path);
    }
    else
    {
      LOG_PRINT_L2("Can't create directory: " << path << ", err: "<< ec.message());
    }

    return res;
  }

  std::error_code replace_file(const std::string& old_name, const std::string& new_name)
  {
    int code;
    bool ok = 0 == std::rename(old_name.c_str(), new_name.c_str());
    code = ok ? 0 : errno;
    return std::error_code(code, std::system_category());
  }

  bool sanitize_locale()
  {
    // std::filesystem throws for "invalid" locales, such as en_US.UTF-8, or kjsdkfs,
    // so reset it here before any calls to it
    try
    {
      std::filesystem::path p {std::string("test")};
      p /= std::string("test");
    }
    catch (...)
    {
#if defined(__MINGW32__) || defined(__MINGW__)
      putenv("LC_ALL=C");
      putenv("LANG=C");
#else
      setenv("LC_ALL", "C", 1);
      setenv("LANG", "C", 1);
#endif
      return true;
    }
    return false;
  }

#ifdef STACK_TRACE
  static void posix_crash_handler(int signal)
  {
    tools::log_stack_trace(("crashing with fatal signal " + std::to_string(signal)).c_str());
#ifdef NDEBUG
    _exit(1);
#else
    abort();
#endif
  }
  static void setup_crash_dump()
  {
    signal(SIGSEGV, posix_crash_handler);
    signal(SIGBUS, posix_crash_handler);
    signal(SIGILL, posix_crash_handler);
    signal(SIGFPE, posix_crash_handler);
  }
#else
  static void setup_crash_dump() {}
#endif

  bool disable_core_dumps()
  {
#ifdef __GLIBC__
    // disable core dumps in release mode
    struct rlimit rlimit;
    rlimit.rlim_cur = rlimit.rlim_max = 0;
    if (setrlimit(RLIMIT_CORE, &rlimit))
    {
      MWARNING("Failed to disable core dumps");
      return false;
    }
#endif
    return true;
  }

  ssize_t get_lockable_memory()
  {
#ifdef __GLIBC__
    struct rlimit rlim;
    if (getrlimit(RLIMIT_MEMLOCK, &rlim) < 0)
    {
      MERROR("Failed to determine the lockable memory limit");
      return -1;
    }
    return rlim.rlim_cur;
#else
    return -1;
#endif
  }

  bool on_startup()
  {
    mlog_configure("", true);

    setup_crash_dump();

    sanitize_locale();

#ifdef __GLIBC__
    const char *ver = gnu_get_libc_version();
    if (!strcmp(ver, "2.25"))
      MCLOG_RED(el::Level::Warning, "global", "Running with glibc " << ver << ", hangs may occur - change glibc version if possible");
#endif

#if OPENSSL_VERSION_NUMBER < 0x10100000 || defined(LIBRESSL_VERSION_TEXT)
    SSL_library_init();
#else
    OPENSSL_init_ssl(0, NULL);
#endif

    return true;
  }
  void set_strict_default_file_permissions(bool strict)
  {
#if defined(__MINGW32__) || defined(__MINGW__)
    // no clue about the odd one out
#else
    mode_t mode = strict ? 077 : 0;
    umask(mode);
#endif
  }

  namespace
  {
    std::mutex max_concurrency_lock;
    unsigned max_concurrency = std::thread::hardware_concurrency();
  }

  void set_max_concurrency(unsigned n)
  {
    if (n < 1)
      n = std::thread::hardware_concurrency();
    unsigned hwc = std::thread::hardware_concurrency();
    if (n > hwc)
      n = hwc;
    std::lock_guard<std::mutex> lock(max_concurrency_lock);
    max_concurrency = n;
  }

  unsigned get_max_concurrency()
  {
    std::lock_guard<std::mutex> lock(max_concurrency_lock);
    return max_concurrency;
  }

  bool is_local_address(const std::string &address)
  {
    // always assume Tor/I2P addresses to be untrusted by default
    if (boost::ends_with(address, ".onion") || boost::ends_with(address, ".i2p"))
    {
      MDEBUG("Address '" << address << "' is Tor/I2P, non local");
      return false;
    }

    // extract host
    epee::net_utils::http::url_content u_c;
    if (!epee::net_utils::parse_url(address, u_c))
    {
      MWARNING("Failed to determine whether address '" << address << "' is local, assuming not");
      return false;
    }
    if (u_c.host.empty())
    {
      MWARNING("Failed to determine whether address '" << address << "' is local, assuming not");
      return false;
    }

    // resolve to IP
    boost::asio::io_service io_service;
    boost::asio::ip::tcp::resolver resolver(io_service);
    boost::asio::ip::tcp::resolver::query query(u_c.host, "");
    boost::asio::ip::tcp::resolver::iterator i = resolver.resolve(query);
    while (i != boost::asio::ip::tcp::resolver::iterator())
    {
      const boost::asio::ip::tcp::endpoint &ep = *i;
      if (ep.address().is_loopback())
      {
        MDEBUG("Address '" << address << "' is local");
        return true;
      }
      ++i;
    }

    MDEBUG("Address '" << address << "' is not local");
    return false;
  }
  int vercmp(const char *v0, const char *v1)
  {
    std::vector<std::string> f0, f1;
    boost::split(f0, v0, boost::is_any_of(".-"));
    boost::split(f1, v1, boost::is_any_of(".-"));
    for (size_t i = 0; i < std::max(f0.size(), f1.size()); ++i) {
      if (i >= f0.size())
        return -1;
      if (i >= f1.size())
        return 1;
      int f0i = atoi(f0[i].c_str()), f1i = atoi(f1[i].c_str());
      int n = f0i - f1i;
      if (n)
        return n;
    }
    return 0;
  }

  std::optional<std::pair<uint32_t, uint32_t>> parse_subaddress_lookahead(const std::string& str)
  {
    auto pos = str.find(":");
    bool r = pos != std::string::npos;
    uint32_t major;
    r = r && epee::string_tools::get_xtype_from_string(major, str.substr(0, pos));
    uint32_t minor;
    r = r && epee::string_tools::get_xtype_from_string(minor, str.substr(pos + 1));
    if (r)
    {
      return std::make_pair(major, minor);
    }
    else
    {
      return {};
    }
  }

  std::string glob_to_regex(const std::string &val)
  {
    std::string newval;

    bool escape = false;
    for (char c: val)
    {
      if (c == '*')
        newval += escape ? "*" : ".*", escape = false;
      else if (c == '?')
        newval += escape ? "?" : ".", escape = false;
      else if (c == '\\')
        newval += '\\', escape = !escape;
      else
        newval += c, escape = false;
    }
    return newval;
  }

  void closefrom(int fd)
  {
#if defined __FreeBSD__ || defined __OpenBSD__ || defined __NetBSD__ || defined __DragonFly__
    ::closefrom(fd);
#else
#if defined __GLIBC__
    const int sc_open_max =  sysconf(_SC_OPEN_MAX);
    const int MAX_FDS = std::min(65536, sc_open_max);
#else
    const int MAX_FDS = 65536;
#endif
    while (fd < MAX_FDS)
    {
      close(fd);
      ++fd;
    }
#endif
  }

  std::string get_human_readable_timestamp(uint64_t ts)
  {
    char buffer[64];
    if (ts < 1234567890)
      return "<unknown>";
    time_t tt = ts;
    struct tm tm;
    misc_utils::get_gmt_time(tt, tm);
    strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &tm);
    return std::string(buffer);
  }

  std::string get_human_readable_timespan(uint64_t seconds)
  {
    if (seconds < 60)
      return std::to_string(seconds) + " seconds";
    std::stringstream ss;
    ss << std::fixed << std::setprecision(1);
    if (seconds < 3600)
    {
      ss << seconds / 60.f;
      return ss.str() + " minutes";
    }
    if (seconds < 3600 * 24)
    {
      ss << seconds / 3600.f;
      return ss.str() + " hours";
    }
    if (seconds < 3600 * 24 * 30.5f)
    {
      ss << seconds / (3600 * 24.f);
      return ss.str() + " days";
    }
    if (seconds < 3600 * 24 * 365.25f)
    {
      ss << seconds / (3600 * 24 * 30.5f);
      return ss.str() + " months";
    }
    if (seconds < 3600 * 24 * 365.25f * 100)
    {
      ss << seconds / (3600 * 24 * 365.25f);
      return ss.str() + " years";
    }
    return "a long time";
  }

  std::string get_human_readable_bytes(uint64_t bytes)
  {
    // Use 1024 for "kilo", 1024*1024 for "mega" and so on instead of the more modern and standard-conforming
    // 1000, 1000*1000 and so on, to be consistent with other Monero code that also uses base 2 units
    struct byte_map
    {
        const char* const format;
        const std::uint64_t bytes;
    };

    static constexpr const byte_map sizes[] =
    {
        {"%.0f B", 1024},
        {"%.2f kB", 1024 * 1024},
        {"%.2f MB", std::uint64_t(1024) * 1024 * 1024},
        {"%.2f GB", std::uint64_t(1024) * 1024 * 1024 * 1024},
        {"%.2f TB", std::uint64_t(1024) * 1024 * 1024 * 1024 * 1024}
    };

    struct bytes_less
    {
        bool operator()(const byte_map& lhs, const byte_map& rhs) const noexcept
        {
            return lhs.bytes < rhs.bytes;
        }
    };

    const auto size = std::upper_bound(
        std::begin(sizes), std::end(sizes) - 1, byte_map{"", bytes}, bytes_less{}
    );
    const std::uint64_t divisor = size->bytes / 1024;
    return (boost::format(size->format) % (double(bytes) / divisor)).str();
  }

  void clear_screen()
  {
    std::cout << "\033[2K" << std::flush; // clear whole line
    std::cout << "\033c" << std::flush; // clear current screen and scrollback
    std::cout << "\033[2J" << std::flush; // clear current screen only, scrollback is still around
    std::cout << "\033[3J" << std::flush; // does nothing, should clear current screen and scrollback
    std::cout << "\033[1;1H" << std::flush; // move cursor top/left
    std::cout << "\r                                                \r" << std::flush; // erase odd chars if the ANSI codes were printed raw
  }

  std::pair<std::string, size_t> get_string_prefix_by_width(const std::string &s, size_t columns)
  {
    std::string sc = "";
    size_t avail = s.size();
    const char *ptr = s.data();
    wint_t cp = 0;
    int bytes = 1;
    size_t sw = 0;
    char wbuf[8], *wptr;
    while (avail--)
    {
      if ((*ptr & 0x80) == 0)
      {
        cp = *ptr++;
        bytes = 1;
      }
      else if ((*ptr & 0xe0) == 0xc0)
      {
        if (avail < 1)
        {
          MERROR("Invalid UTF-8");
          return std::make_pair(s, s.size());
        }
        cp = (*ptr++ & 0x1f) << 6;
        cp |= *ptr++ & 0x3f;
        --avail;
        bytes = 2;
      }
      else if ((*ptr & 0xf0) == 0xe0)
      {
        if (avail < 2)
        {
          MERROR("Invalid UTF-8");
          return std::make_pair(s, s.size());
        }
        cp = (*ptr++ & 0xf) << 12;
        cp |= (*ptr++ & 0x3f) << 6;
        cp |= *ptr++ & 0x3f;
        avail -= 2;
        bytes = 3;
      }
      else if ((*ptr & 0xf8) == 0xf0)
      {
        if (avail < 3)
        {
          MERROR("Invalid UTF-8");
          return std::make_pair(s, s.size());
        }
        cp = (*ptr++ & 0x7) << 18;
        cp |= (*ptr++ & 0x3f) << 12;
        cp |= (*ptr++ & 0x3f) << 6;
        cp |= *ptr++ & 0x3f;
        avail -= 3;
        bytes = 4;
      }
      else
      {
        MERROR("Invalid UTF-8");
        return std::make_pair(s, s.size());
      }

      wptr = wbuf;
      switch (bytes)
      {
        case 1: *wptr++ = cp; break;
        case 2: *wptr++ = 0xc0 | (cp >> 6); *wptr++ = 0x80 | (cp & 0x3f); break;
        case 3: *wptr++ = 0xe0 | (cp >> 12); *wptr++ = 0x80 | ((cp >> 6) & 0x3f); *wptr++ = 0x80 | (cp & 0x3f); break;
        case 4: *wptr++ = 0xf0 | (cp >> 18); *wptr++ = 0x80 | ((cp >> 12) & 0x3f); *wptr++ = 0x80 | ((cp >> 6) & 0x3f); *wptr++ = 0x80 | (cp & 0x3f); break;
        default: MERROR("Invalid UTF-8"); return std::make_pair(s, s.size());
      }
      *wptr = 0;
      sc += std::string(wbuf, bytes);
      int cpw = wcwidth(cp);
      if (cpw > 0)
      {
        if (cpw > (int)columns)
          break;
        columns -= cpw;
        sw += cpw;
      }
      cp = 0;
      bytes = 1;
    }
    return std::make_pair(sc, sw);
  }

  size_t get_string_width(const std::string &s)
  {
    return get_string_prefix_by_width(s, 999999999).second;
  };

  std::vector<std::pair<std::string, size_t>> split_line_by_width(const std::string &s, size_t columns)
  {
    std::vector<std::string> words;
    std::vector<std::pair<std::string, size_t>> lines;
    boost::split(words, s, boost::is_any_of(" "), boost::token_compress_on);
    // split large "words"
    for (size_t i = 0; i < words.size(); ++i)
    {
      for (;;)
      {
        std::string prefix = get_string_prefix_by_width(words[i], columns).first;
        if (prefix == words[i])
          break;
        words[i] = words[i].substr(prefix.size());
        words.insert(words.begin() + i, prefix);
      }
    }

    lines.push_back(std::make_pair("", 0));
    while (!words.empty())
    {
      const size_t word_len = get_string_width(words.front());
      size_t line_len = get_string_width(lines.back().first);
      if (line_len > 0 && line_len + 1 + word_len > columns)
      {
        lines.push_back(std::make_pair("", 0));
        line_len = 0;
      }
      if (line_len > 0)
      {
        lines.back().first += " ";
        lines.back().second++;
      }
      lines.back().first += words.front();
      lines.back().second += word_len;
      words.erase(words.begin());
    }
    return lines;
  }

  // Calculate a "sync weight" over ranges of blocks in the blockchain, suitable for
  // calculating sync time estimates
  uint64_t cumulative_block_sync_weight(cryptonote::network_type nettype, uint64_t start_block, uint64_t num_blocks)
  {
    return num_blocks;
  }

  std::vector<std::pair<std::string, size_t>> split_string_by_width(const std::string &s, size_t columns)
  {
    std::vector<std::string> lines;
    std::vector<std::pair<std::string, size_t>> all_lines;
    boost::split(lines, s, boost::is_any_of("\n"), boost::token_compress_on);
    for (const auto &e: lines)
    {
      std::vector<std::pair<std::string, size_t>> new_lines = split_line_by_width(e, columns);
      for (auto &l: new_lines)
        all_lines.push_back(std::move(l));
    }
    return all_lines;
  }
}
